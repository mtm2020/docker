FROM masterthemainframe/ansible:latest

ENV VARTEST="TEST"

# Keep Docker Containers Running
CMD tail -f /dev/null
